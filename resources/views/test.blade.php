<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel='stylesheet' href='css/app.css' type='text/css' media='all' />
        <!-- <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css"> -->
        <link rel="stylesheet" type="text/css" href="http://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/css/jquery.dataTables.css">  
        <link rel='stylesheet' href='https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css' type='text/css' media='all' />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" crossorigin="anonymous">
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
   
        <!-- Styles -->
        <style>
        </style>
    </head>
    <body>
        <div style="margin: auto; width:100%">
            <div class="row">
                <div class="col-md-3"><span>Add Row</span></div>
            </div>
            <div class="row col-12">
                <div class="col-md-2 form-group">
                    <label for="code_contract">Department in charge</label>
                    <select name="name_product" id="select" class="form-control select2" data-placeholder="Select" >
                        <option value="">All</option>
                    </select>
                </div>
               
                <div class="input_1 col-md-2 form-group">
                    <label for="code_contract">Search ID</label>
                    <input type="text" placeholder=" Search ID" />
                </div>

                <div class="input_2 col-md-2 form-group">
                    <label for="code_contract">Search Name</label>
                    <input type="text" placeholder=" Search Name" />
                </div>
            </div>
                
                <hr>
            <table id="myTable" class="table table-striped table-bordered row-border hover order-column" border="1">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Department <br/>in charge</th>
                        <th>A/C Code</th>
                        <th>Group PL</th>
                        <th>A/C Name</th>
                        <th>B2019</th>
                        <th>LE2018 Last Estimation</th>
                        <th>Delta</th>
                        <th>%</th>
                        <th>Comment</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
        <div style="text-align: right">
            <div>Total Quantity:<span class="total_quantity"></span> </div>
            <div>Total Price:<span class="total_price"></span></div>
        </div>
         <!-- jQuery -->
        <script src="//code.jquery.com/jquery.js"></script>
        <!-- DataTables -->
        <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
        <!-- Bootstrap JavaScript -->
        <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
        <!-- App scripts -->
        <script src="js/test.js"></script>
        <script>

        </script>
    </body>
</html>
