$(document).ready(function () {
    var table_products = $('#myTable').DataTable(
        {
            "scrollY": "400px",
            "scrollCollapse": true,
            "paging": false,
            "bSort": false,
            "filter": false,
            "info": false,
            "lengthChange": false,
            "searching": true,
            "columnDefs": [
                { "width": '15px', "targets": 0, "orderable": false },
                { "targets": 2, "orderable": false },
                { "className": "dt-right", "targets": [3, 4] },
                // {"targets": 4, "visible": false}
            ],
            "language": {
                "decimal": ",",
                "thousands": "."
            }
        });
    var data = [
        {
            id: 0,
            name: 'Product 1',
            quantity: 12,
            price: 3000,
        },
        {
            id: 0,
            name: 'Product 2',
            quantity: 11,
            price: 3000,
        },
        {
            id: 0,
            name: 'Product 3',
            quantity: 13,
            price: 3000,
        }, {
            id: 0,
            name: 'Product 4',
            quantity: 14,
            price: 3000,
        },
        {
            id: 0,
            name: 'Product 5',
            quantity: 15,
            price: 3000,
        },
        {
            id: 0, 
            name: 'Product 6',
            quantity: 16,
            price: 3000,
        },
        {
            id: 0,
            name: 'Product 7',
            quantity: 17,
            price: 3000,
        },
        {
            id: 0,
            name: 'Product 9',
            quantity: 18,
            price: 3000,
        },
        {
            id: 0,
            name: 'Product 10',
            quantity: 19,
            price: 3000,
        }, {
            id: 0,
            name: 'Product 11',
            quantity: 9,
            price: 3000,
        },
        {
            id: 0,
            name: 'Product 12',
            quantity: 8,
            price: 3000,
        },
        {
            id: 0, 
            name: 'Product 13',
            quantity: 3,
            price: 3000,
        },
        {
            id: 0, 
            name: 'Product 13',
            quantity: 3,
            price: 6000,
        }
    ]

    let totalQuantity = 0;
    var totalPrice = 0;
    let select = [];
    for (let i = 0; i < data.length; i++) {
        var item = data[i];
        let total = item.quantity * item.price
        totalQuantity += item.quantity;
        totalPrice += total;
        table_products.row.add([
            i + 1,
            item.name,
            '<input class="form-control quantity number text-right" value="' + item.quantity + '">',
            '<input class="form-control price number text-right" value="' + formatNumber(item.price) + '">',
            formatNumber(total),
            total,
            total,
            total,
            total,
            total,
        ]);
        select.push(item.name);
        jQuery.unique(select);
    }

    for (let i = 0 ; i < select.length; i++)
    {
        var z = document.createElement("option");
        z.setAttribute("value", select[i]);
        var t = document.createTextNode(select[i]);
        z.appendChild(t);
        document.getElementById("select").appendChild(z);
    }

    table_products.draw(false);
    $('.total_quantity').text(formatNumber(totalQuantity));
    $('.total_price').text(formatNumber(totalPrice));

    /** keyup input text quantity*/
    $('body').on('keyup', '.quantity', function () {
        var price = $("#myTable").find('tbody').find('tr').eq($(this).parents('tr').index()).find('td:eq(3)').find('input').val();
        let sefl = $(this).val();
        let total = unFormatNumber(price) * sefl;
        $("#myTable").find('tbody').find('tr').eq($(this).parents('tr').index()).find('td:eq(4)').text(formatNumber(total));
        priceCallback();
        quantityCallback();
    });
    /** keyup input text price*/
    $('body').on('keyup', '.price', function () {
        var quantity_input = $("#myTable").find('tbody').find('tr').eq($(this).parents('tr').index()).find('td:eq(2)').find('input').val();
        let sefl = unFormatNumber($(this).val());
        let total = quantity_input * sefl;
        $("#myTable").find('tbody').find('tr').eq($(this).parents('tr').index()).find('td:eq(3)').find('input').val(formatNumber(sefl));
        $("#myTable").find('tbody').find('tr').eq($(this).parents('tr').index()).find('td:eq(4)').text(formatNumber(total));
        priceCallback();
        quantityCallback();
    });

    /** total price*/
    function priceCallback() {
        var sum = 0
        $('#myTable').find('tbody tr').find('td:eq(4)').each(function () {
            let sefl = unFormatNumber($(this).text());
            sum += parseFloat(sefl)
        })
        $(".total_price").html(formatNumber(sum));
    }
    $('#myTable tbody').on('click', 'tr','td', function () {

    /** total quantity*/
    function quantityCallback() {
        let sum = 0
        $('#myTable').find('tbody tr').find('td:eq(2)').each(function () {
            let valInput = $(this).find("input").val();
            let sefl = unFormatNumber(valInput);
            sum += parseFloat(sefl)
        })
        $(".total_quantity").html(formatNumber(sum));
        

    }
    function formatNumber(num) {
        return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
    }
    function unFormatNumber(num) {
        num = num.toString().replace(",","");
        num = num.toString().replace(",","");
        num = num.toString().replace(",","");
        return num.toString().replace(",", "");
    }
        var data = table_products.row( this ).data();
        var dataCell = table_products.cell( this ).data();
         alert( 'You clicked on '+dataCell+'\'s '+data[1]+' row' );
    } );
    $('#myTable tbody').on( 'mouseenter', 'td', function () {
        var colIdx = table_products.cell(this).index().column;
        $( table_products.cells().nodes() ).removeClass( 'highlight' );
        $( table_products.column( colIdx ).nodes() ).addClass( 'highlight' );
    } );

    $('.input_1 input').unbind().bind('keyup', function() {
        table_products.column( 0).search( this.value ).draw();
    });

    $('.input_2 input').unbind().bind('keyup', function() {
        table_products.column( 1).search( this.value ).draw();
    });

    $('#select').on('change' ,function() {
        table_products.column( 1).search( this.value ).draw();
    });

    
});